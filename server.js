const express = require('express')
const mysql = require('mysql')
const cors = require('cors');
const bodyparser = require('body-parser');

function openDatabaseConnection() {

  const connection = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'manager',
    database: 'dec10',
  })

  connection.connect()
  return connection
}

const app = express()
app.use(express.json())
app.use(cors());
app.use(bodyparser.json());

//---------------------------------------------------------------------------------------------------------------------------------------------------

app.get('/Student/:roll_no', (request, response) => {
  const { roll_no } = request.params
const connection = openDatabaseConnection()
const statement = `select  roll_no, Student_name, Class, division, dateofbirth,parent_mobile_no
 from Student where roll_no=${roll_no}`
connection.query(statement, (error, data) => {
connection.end()

  if (error) {
  
    response.send(error)
  } else {  
    response.send(data)
  }
})
})


//-----------------------------------------------------------------------------------------------------------------------------------------------------

//roll_no | Student_name | class | division | dateofbirth | parent_mobile_no



app.post('/Student/', (request, response) => {
  const { roll_no, Student_name, Class, division,dateofbirth, parent_mobile_no  } = request.body
  const connection = openDatabaseConnection()
  const statement = ` insert into Student (roll_no, Student_name, Class, division,  dateofbirth, parent_mobile_no )
  values('${roll_no}','${Student_name}','${Class}','${division}','${dateofbirth}','${parent_mobile_no}')`

  connection.query(statement, (error, data) => {
  connection.end()
  
    if (error) {
    
      response.send(error)
    } else {  
      response.send(data)
    }
  })
  })

  //-------------------------------------------------------------------------------------------------------------------------------------------------

  app.put('/Student/:roll_no', (request, response) => {
    const { Class, division } = request.body
    const { roll_no } = request.params
    const connection = openDatabaseConnection()
    const statement = `update Student set Class='${Class}', division='${division}'
    where roll_no=${roll_no}`
  
    connection.query(statement, (error, data) => {
    connection.end()
    
      if (error) {
      
        response.send(error)
      } else {  
        response.send(data)
      }
    })
    })

//--------------------------------------------------------------------------------------------------------------------------------------------------

    app.delete('/Student/:roll_no', (request, response) => {
    
      const { roll_no } = request.params
      const connection = openDatabaseConnection()
      const statement = `delete from  Student 
      where roll_no=${roll_no}`
    
      connection.query(statement, (error, data) => {
      connection.end()
      
        if (error) {
        
          response.send(error)
        } else {  
          response.send(data)
        }
      })
      })


//------------------------------------------------------------------------------------------------------------------------------------------------------------
 
      app.get('/Student_c/:Class', (request, response) => {
        const { Class } = request.params
      const connection = openDatabaseConnection()
      const statement = `select  * from Student where Class='${Class}'`
      connection.query(statement, (error, data) => {
      connection.end()
      
        if (error) {
        
          response.send(error)
        } else {  
          response.send(data)
        }
      })
      })

//---------------------------------------------------------------------------------------------------------------------------------------------------------

    
      app.get('/Student_dob/:dateofbirth', (request, response)=>{
        const connection = openDatabaseConnection()
        console.log(request.params.dateofbirth);
        let sql = `SELECT * FROM student WHERE YEAR(dateofbirth) = '${request.params.dateofbirth}'`;
        console.log(sql)
        connection.query(sql, (error, data) => {
          connection.end()
          if (error) {
                response.send(error)
              } else {  
                response.send(data)
              }
        })
    });





app.listen(4000, () => {
    console.log('server started on port 4000')
  })